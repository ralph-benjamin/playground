<!DOCTYPE html>
<html lang="en">
<head>
	@include('partials.header_scripts')
	<meta charset="UTF-8">
	<title>Playground</title>
</head>
<body>
	<div class="container">
    	@yield('content')
    </div>
	@include('partials.footer_scripts')
</body>
</html>